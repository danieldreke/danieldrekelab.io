---
title: Access credit card money
subtitle: Transfer credit to your bank account via TransferWise
date: 2019-08-31
draft: true
tags: [transferwise, moneytransfer, creditcard, money]
---

With a credit card you can pay cashless.
Get cash via an ATM.
Pay online.
Internationally.
At no costs with a [DKB VISA card](https://www.dkb.de/privatkunden/visa-card/).
But what if you want money from your credit card on your bank account?
I struggled. 
Then I remembered the international money transfer service [TransferWise](https://en.wikipedia.org/wiki/TransferWise). 
Depending on your limits you can transfer up to 2500 EUR for less then 3 EUR transfer fee if you transfer to an EUR bank account.
Which is like a very good credit deal of less than 0.3% interest fee for up to 30 days. 

If you have a [Comdirect VISA card](https://www.comdirect.de/konto/karten.html#Visa-Karte) be aware of a possible limit and consequently refusal of the transfer. 
Mine was 600 EUR despite different limit settings.
In that case you have to wait until next day to be able to transfer again.
I didn't have that problem with my DKB VISA card in case you have one.
Be also aware of your monthly credit card limit.

Besides that I can also recommend TransferWise for fast and easy international money transfers. 
Transaction took just seconds to arrive in an Indian INR account for less than 1% transfer fee.
Impressive.
I remember how in 2015 a friend from Malaysia struggled for months to send me money from his Malaysian bank account and how we ended up with a ridiculously high transfer fee of 33% or around 46 EUR for transferring 140 EUR. The shitty bank goes by the name United Overseas Bank Malaysia in case you are interested.

If you enjoyed reading this post feel free to register for TransferWise via [my invite link](https://transferwise.com/u/danield2556) giving you an international transfer up to 500 EUR for free, saving you 17 EUR if Chile is your country of choice.
You can also register via [transferwise.com/register](https://transferwise.com/register). 

Thank you for reading.<br>
Daniel

PS: Popped up while writing: Whatever you give is a gift to yourself. Thank you ♡ Anita ♡