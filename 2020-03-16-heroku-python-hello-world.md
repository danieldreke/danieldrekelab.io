---
title: "Hello world" and Heroku
subtitle: Getting to know Heroku via "hello world" example
date: 2020-03-16
draft: false
tags: [heroku, python, hello world, webapp]
---

# [TODO] create repo with all files
# [TODO] create deploy Heroku button

https://vimeo.com/147784643

    # sudo apt install virtualenv
    virtualenv venv
    . venv/bin/activate
    # pip install --upgrade setuptools
    pip install requests[security]
    pip install flask
    pip install gunicorn
    pip freeze > requirements.txt
    # remove everything except [TODO] link to file
    touch helloworld.py Procfile
    atom helloworld.py
    # copypaste from [TODO] / show code and link to file
    mkdir heroku-python-hello-world
    git init
    git status
    git add helloworld.py requirements.txt Procfile
    # sudo apt install snapd
    # sudo snap install heroku --classic
    # if heroku cannot be found after install https://stackoverflow.com/a/12845896
    # sudo ln -s /snap/bin/heroku /usr/bin/heroku
    heroku login
    heroku create heroku-python-hello-world
    git push heroku master
